### [1.0.6](https://gitlab.com/goopil/flux-multi-mr/example-app/compare/develop-1.0.5...develop-1.0.6) (2024-01-21)


### Bug Fixes

* improve rule filter ([8c7df01](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/8c7df0155436134a6bf71e2e377081bffa2663c9))

### [1.0.5](https://gitlab.com/goopil/flux-multi-mr/example-app/compare/develop-1.0.4...develop-1.0.5) (2024-01-21)


### Bug Fixes

* add missing title ([ef0e67f](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/ef0e67f3a0095224c98b9376c3be7e9ce21d642b))

### [1.0.4](https://gitlab.com/goopil/flux-multi-mr/example-app/compare/develop-1.0.3...develop-1.0.4) (2024-01-21)


### Bug Fixes

* env substitution ([5fb5d7f](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/5fb5d7f11078dc88be081f58f4ec06843df9ab34))

### [1.0.3](https://gitlab.com/goopil/flux-multi-mr/example-app/compare/develop-1.0.2...develop-1.0.3) (2024-01-21)


### Bug Fixes

* curl ([a55416b](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/a55416b8ba7287ddf6d0b7c1bbbbca696212291d))

### [1.0.2](https://gitlab.com/goopil/flux-multi-mr/example-app/compare/develop-1.0.1...develop-1.0.2) (2024-01-21)


### Bug Fixes

* trigger mr open ([c087795](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/c0877959233298b79823cbc4c3433a7b2d783cbc))

### [1.0.1](https://gitlab.com/goopil/flux-multi-mr/example-app/compare/develop-1.0.0...develop-1.0.1) (2024-01-21)


### Bug Fixes

* adjust when pipeline are triggered ([caec4e3](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/caec4e3e1a6ffb3d31259198dc632b08f911870e))
* move to js config file ([77414bb](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/77414bb1ada5e359787d28e215c95ac2b8c7c050))

## 1.0.0 (2024-01-21)


### Features

* add deploy between develop & master ([925c421](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/925c4214fcd8d6e30c56961113cb078ffca8a954))
* add semantic release ([5e5c295](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/5e5c295c6ac1ab8bb5a8aa41453c7dc972609267))
* try adding production channel ([6623ed8](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/6623ed87db809d0dc6c160b67bac0b5ef8f2b2fa))


### Bug Fixes

* add injected branch ([7453d34](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/7453d34e75aecdcd5ad619140dbbb78ef4d06dab))
* cache disable def ([18258a1](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/18258a1621a0405a68ddc60426fdd96405f91c3d))
* commit tag detection ([7430bec](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/7430bec68b7e1b747c924897cda06c8aaf00702b))
* debug all env var ([d0f4fae](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/d0f4fae20bcc8f6bc6d537e27199ea677593f153))
* fix rules ([762c7a7](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/762c7a76b15b9f8d63c2b87b87b142bf098c62f5))
* rules definition ([b1605f8](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/b1605f85f97e06e2a960f0e019b190b0bebf1c82))

### [1.2.4](https://gitlab.com/goopil/flux-multi-mr/example-app/compare/v1.2.3...v1.2.4) (2024-01-21)


### Bug Fixes

* debug all env var ([d0f4fae](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/d0f4fae20bcc8f6bc6d537e27199ea677593f153))

### [1.2.3](https://gitlab.com/goopil/flux-multi-mr/example-app/compare/v1.2.2...v1.2.3) (2024-01-21)


### Bug Fixes

* commit tag detection ([7430bec](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/7430bec68b7e1b747c924897cda06c8aaf00702b))

### [1.2.2](https://gitlab.com/goopil/flux-multi-mr/example-app/compare/v1.2.1...v1.2.2) (2024-01-21)


### Bug Fixes

* fix rules ([762c7a7](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/762c7a76b15b9f8d63c2b87b87b142bf098c62f5))

### [1.2.1](https://gitlab.com/goopil/flux-multi-mr/example-app/compare/v1.2.0...v1.2.1) (2024-01-21)


### Bug Fixes

* cache disable def ([18258a1](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/18258a1621a0405a68ddc60426fdd96405f91c3d))

### [1.2.1-rc.1](https://gitlab.com/goopil/flux-multi-mr/example-app/compare/v1.2.0...v1.2.1-rc.1) (2024-01-21)


### Bug Fixes

* cache disable def ([18258a1](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/18258a1621a0405a68ddc60426fdd96405f91c3d))

## [1.2.0](https://gitlab.com/goopil/flux-multi-mr/example-app/compare/v1.1.0...v1.2.0) (2024-01-21)


### Features

* add deploy between develop & master ([925c421](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/925c4214fcd8d6e30c56961113cb078ffca8a954))


### Bug Fixes

* rules definition ([b1605f8](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/b1605f85f97e06e2a960f0e019b190b0bebf1c82))

## [1.1.0](https://gitlab.com/goopil/flux-multi-mr/example-app/compare/v1.0.0...v1.1.0) (2024-01-21)


### Features

* try adding production channel ([6623ed8](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/6623ed87db809d0dc6c160b67bac0b5ef8f2b2fa))

## 1.0.0 (2024-01-21)


### Features

* add semantic release ([5e5c295](https://gitlab.com/goopil/flux-multi-mr/example-app/commit/5e5c295c6ac1ab8bb5a8aa41453c7dc972609267))
