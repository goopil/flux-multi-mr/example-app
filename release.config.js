module.exports = {
  tagFormat: process.env.CI_COMMIT_BRANCH
    ? process.env.CI_COMMIT_BRANCH !== 'master'
      ? '${process.env.CI_COMMIT_BRANCH}-${version}'
      : 'v${version}'
    : 'v${version}',

  branches: [
    {name: 'master'},
    {name: 'develop'}
  ],

  plugins: [
    [
      '@semantic-release/commit-analyzer',
      {
        preset: 'conventionalcommits'
      }
    ],
    [
      '@semantic-release/release-notes-generator',
      {
        preset: 'conventionalcommits'
      }
    ],
    // [
    //   '@semantic-release/changelog',
    //   {
    //     // changelogFile: 'docs/CHANGELOG.md'
    //   }
    // ],
    [
      '@semantic-release/git',
      {
        // assets: ['docs/CHANGELOG.md'],
        message: "chore(release): ${nextRelease.version}\n\n${nextRelease.notes}"
      }
    ],
    '@semantic-release/gitlab',
  ]
}
